---
- name: list previously installed components
  shell: |
    set -o pipefail && helm list -a | awk '{print $1}' | grep {{ chart_name }}- || true
  args:
    executable: /bin/bash
  register: components
  changed_when: "false"

- name: remove previously installed components
  command:
    "helm undeploy {{ item }} --purge"
  loop: "{{ components.stdout_lines }}"
  register: helm_undeploy
  async: 900
  poll: 0

- name: Wait for component deletion
  async_status:
    jid: "{{ item.ansible_job_id }}"
  register: _jobs
  until: _jobs.finished
  delay: 5
  retries: 300 
  loop: "{{ helm_undeploy.results }}"
  loop_control:
    label: "{{ item.item }}"

- name: check if an onap installation has been launched before
  shell: |
    set -o pipefail && helm list -a | awk '{print $1}' | grep -c {{ chart_name }} || true
  args:
    executable: /bin/bash
  register: launched
  changed_when: "false"

- name: remove previous installation
  command:
    "helm undeploy {{ chart_name }} --purge"
  when: launched.stdout != '0'

- name: get number of remaining pods
  command: >
    kubectl get pods --namespace {{ onap_namespace }} --no-headers
    -o custom-columns=NAME:.metadata.name
  changed_when: False
  register: pods

- name: delete remaining faulty pods
  command: >
    kubectl delete pods --namespace {{ onap_namespace }} --force
    --grace-period 0 {{ item }}
  loop: "{{ pods.stdout_lines }}"
  when: (pods.stdout_lines | length) <= (faulty_pods | length) and
        ((item | regex_replace('^[a-zA-Z0-9]+-') |
                 regex_replace('-[0-9a-z]+-[0-9a-z]+$')) in faulty_pods)

- name: check if namespace is for namespace full deletion
  shell: |
    set -o pipefail && kubectl get namespace {{ onap_namespace }}
    -o jsonpath="{.status.phase}" || true
  args:
    executable: /bin/bash
  register: ns_status
  ignore_errors: yes

- name: delete onap namespace
  k8s:
    state: absent
    definition:
      apiVersion: v1
      kind: Namespace
      metadata:
        name: "{{ onap_namespace }}"
  when: (not ns_status.failed) and ('Terminating' not in ns_status.stdout)

- name: wait for namespace full deletion
  shell: |
    set -o pipefail && kubectl get namespace |
    grep -c {{ onap_namespace }} || true
  args:
    executable: /bin/bash
  register: kube
  changed_when:
    kube.stdout  == '0'
  until:
    kube.stdout  == '0'
  retries: 600
  delay: 1

- name: list all remaining persistent volumes
  shell: |
    set -o pipefail &&
    kubectl get pv -o=jsonpath='{range .items[*]}{.metadata.name}{"\n"}{end}' |
    grep {{ chart_name }} || true
  args:
    executable: /bin/bash
  register: persistent_volumes
  changed_when: "false"

- name: see output
  debug:
    msg: "{{ persistent_volumes.stdout }}"

- name: remove remaining persistent volumes
  shell: |
    set -o pipefail && kubectl delete pv {{ item }} || true
  args:
    executable: /bin/bash
  changed_when: "true"
  loop: "{{ persistent_volumes.stdout_lines }}"

- name: "list all onap directories in {{ nfs_folder }}"
  find:
    paths: "{{ nfs_folder }}"
    recurse: no
    file_type: directory
  register: onap_directories

- name: "delete onap directory in {{ nfs_folder }}"
  become: "yes"
  file:
    path: "{{ item.path }}"
    state: absent
  loop: "{{ onap_directories.files }}"
  loop_control:
    label: "{{ item.path }}"

- name: delete componet-gating-overrides.yaml if present
  file:
    path: "{{ override_gating_component }}"
    state: absent
